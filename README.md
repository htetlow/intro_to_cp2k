# Introduction to CP2K

* Holly Judge - tutor
* Julien Sindt, Michael Bareford - helpers

## About this course

CP2K is a quantum chemistry package for performing atomistic simulations which 
has a variety of applications and features. CP2K is used for wide variety of 
systems such as solid-state systems, molecules, liquids and biological systems. 
It is optimized for using density functional theory (DFT) with the mixed Gaussian 
and Plane-Waves (GPW) method based on pseudopotentials.

This course is designed to teach attendees how to run basic CP2K calculations 
and give them the key knowledge required for doing so. It will cover the
important options in the CP2K input file and how to set up a CP2K calculation
before going through some hands-on practical exercises.

We will also cover a selection of helpful tips and how to start with 
using CP2K for your own system. This will include
strategies for creating your own input file and where to find guidance and help 
when preparing input files.

This course will assume no prior experience of using CP2K or other atomistic 
simulation packages however it will also aim to be useful to those who have some
basic experience of using CP2K. We expect attendees to have some understanding
of the key theoretical methods used in CP2K (i.e. density functional theory and
electronic structure calculations) and experience of using ssh, the command line
and some familiarity with using HPC machines (such as ARCHER2).

While we will be referencing density functional theory and other theoretical
methods understanding of these is not the principal aim of this course. Our aim
is to help familiarise attendees with using CP2K to run atomistic simulations.

The main topics covered will be:

* What can CP2K do
* Understanding the CP2K input file
* Input file preparation
* Running CP2K jobs and understanding the output
* Key parameters and ensuring accuracy
* Getting good performance
* How to start with running your own simulations and where to find guidance
* Practicals: Energy minimisation, energy cut-off convergence, geometry optimisation and molecular dynamics


## Aims

* Understand the CP2K input file and how to set key paramters
* Become familiar with running some CP2K calculations
* Learn how to get performance when running CP2K
* Know how to go about setting up your own system

## Course Schedule

### Day 1

* 13:00 - Welcome and introduction
* 13:15 - The CP2K input file and running jobs
* 13:45 - exercise 1: energy calculation
* 14:30 - break
* 15:00 - exercise 2: converging the energy cutoff
* 16:00 - close

### Day 2

* 13:00 - Welcome and introduction
* 13:10 - exercise 3: geometry optimisation
* 13:45 - exercise 4: molecular dynamics
* 14:15 - break
* 14:45 - exercise 5: parallel performance of CP2K
* 15:45 - Where next? Making your own inputs and getting help
* 16:00 - close

## Anknowledgements

The materials here are based on the CP2K HowTo guide - https://www.cp2k.org/howto
